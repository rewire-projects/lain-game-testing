# Make sure you read the RenPy documentation before you get started
# https://www.renpy.org/doc/html/index.html

# Game Script

define l = Character("Lain")

# The game starts here
label start:

    # modd.mp3 starts playing
    #(modd - Mist of a Different Dimension - From the Serial Experiments Lain Soundtrack album)
    play music "audio/modd.mp3"

    # static background appears
    scene bg_static
    with fade

    # Dialogue starts here
    "?" "Hello"

    "?" "Can you see me?"

    "?" "..."

    # Lain appears here
    # The main sprite, like every sprite in the project, is a placeholder
    show lain_main
    with dissolve

    l "Ah... Hello there!"

    l "My name is Lain."

    hide lain
    with dissolve
    scene black
    with fade

    # This ends the game.
    return
